# **Listening and Active Communication**

## **Active Listening**

---

Question : What are the steps/strategies to do Active Listening?

- Avoid getting distracted by your own thoughts
- Try not to interrupt other person
- Use door openers that shows you are interested and keep the other person talking
- Use you body language
- Take notes for important conversation
- Do response by words when needed.

## **Reflective Listening**

---

Question : According to Fisher's model, what are the key points of Reflective Listening?

- It help the other focus on self, sort out issues, express feelings,and deal more effectievly with emotions.
- It help other arrive at a solution to his or her own problem.
- It helps you deal effectively with the issue, problem and/or needs the other raised.
- It avoids the illusion of understanding.

## **Reflaction**

---

Question : What are the obstacles in your listening process?

- Easily get disconnected from any conversation if I don't like the topic.
- Not writing notes.
- If I already have a knowledge about that topic.

Question : What can you do to impove your listening?

- Do focusing on linstening.
- Try to make communication with speaker
- Write notes
- Use body language

## **Types of communication**

---

Question : When do you switch to passive communication style in your day to day life?

- When things are not in my hands
- When I want some situation to let it be

Question : When do you switch to Aggresive communication styles in your day to day life?

- When people behaves badely with animals, birds and people.
- When people are useing other people just for their need.

Question: When do you switch into Passive Aggrasive communication styles in your day to day life?

- When someone taking my words lightely.
- When someone tries to putting me down because he/she wants his/her self up.

Question: How can you make your communication assertive?

- Find what I feel about others and do expression based on that.
- Find what I need.
- Start to communicate with low-stakes situation
- Improve body language.
