# **Learning Process**

## **How to Learn Faster with the Feynman Technique**

---

**Question 1**

What is the Feynman Technique? Paraphrase the video in your own words.

**Answer 1**

When you explain any topic in simple language, you realize how much knowledge you have about that topic. You will also be able to notice your problem areas because at that point you don't explain more or you use complex language.

This is the Idea of the Feynman Technique. The more you simpler your topic you will get knowledge from it. By using the Feynman Technique you can learn any complex problem in a simple way. When you start writing about any topic in your simple language and you are stuck at any point you realize that you don't have much knowledge about that topic. Try to learn that topic deeply and write it again. It is a very simple technique but it will help you to learn any topic fast and efficiently.

**Question 2**

What are the different ways to implement this technique in your learning process?

**Answer 2**

The basic thing about this technique is to explain the topic which you want to learn. When we learn any topic to explain we must learn deeply because we have to be ready for the questions that may arise at the time of explaining. We can execute Feynman Technique in a number of ways.

- Try to explain your topic to your friends,students,kids or anyone, but maybe this can't be possible every time.
- In a second way, you just need a sheet of paper. First, write the topic name on the top of the sheet. Second, write the content in simple language about that topic. You can use any style to represent your topic. Third, review your explanation and identify where you don't use many words to explain or where you use complex language. Fourth, rewrite that topic after learning it again.

Make sure that whoever person will read your topic, it must be understandable by that person whether the person has knowledge about that topic or not.

## **Learning How to Learn TED talk by Barbara Oakley**

---

**Question 3**

Paraphrase the video in detail in your own words.

**Answer 3**

If you focus on your work you will be a masterpiece in your field. But to do focus on work we need to handle our brain fundamentally in two different modes. one is focus and the other is defuse mode. We can use our focus mode when we are familiar with that topic and when we have stuck anywhere or trying to understand a new topic use our defuse mode. When I am stuck anywhere I use to listen to songs as my defuse mode which gives me new ideas and relaxes my mind.  
Everybody can use his/her defuse mode in different ways.

There are two types of people who see their problems in different ways. One people who see their problem, fix their mind to solve it, and focuses on that. And the other people who put their work to do it another day just because they don't want to do it. There are also some people who take much time to understand the topic which doesn't mean they are not intelligent but they are creative. With a slow thinker, you may get solid mastery of what you are studying. There are many ways to improve your learning process is set a timer, relaxing, exercising, testing, recalling, learning alone, and most important focusing.

**Question 4**

What are some of the steps that you can take to improve your learning process?

**Answer 4**

We can use many ways to imporove our learning process which are described below:

- Through Focus and Relax mode. In this mode, you just need to require focus. But in some situations, if you are stuck anywhere or trying to learn a new thing try to use a relaxed mode to understand a new topic.
- Set the timer to do anything but with focus mode. Some people do their work the whole day but didn't get what they learned. But If you set a timer of some time with a focus which means no other task, gives you the best result.
- Relaxation is also needed for the process of learning. Your mind needs to be relaxed after doing some focused work.
- Exercise can increase our ability to both learn and remember.
- Test yourself. The test is the best practice for the learning process. It improves your knowledge on that topic if you test yourself.
- The recall is also the procedure for the learning process. Try to recall what you have learned. Don't highlight anything just look at a page, look away, and see what your can recall.
- Try to learn the thing alone. Understanding alone is the master of material but combine with practice and repetition.

## **Learn anything in 20 hours**

---

**Question 5**

Your key takeaways from the video? Paraphrase your understanding.

**Answer 5**

The key takeaway from this video by my side is you can learn anything. You just need to require only 20 hours of focus deliberate practice on that topic. The more time you spend, the better you get.

If you want to learn anything it may be language, computer language, any musical instruments, or any invest your 20 hours in the most effective way that you possibly can and you will be astounded. The most important thing to achieve this is practice. Without practice, you can't achieve anything. But the question arises is that many people do practice but all don't get achievements because they are not doing practice in the right way.

There are a few steps we have to follow to learn in 20 hours. The first step is to deconstruct the skill. Part your skill into small pieces and decide what part of the skill is actually useful to get your target. With this, you will be able to solve any complex problems and improve your performance. The second is to learn enough to self-correct. Don't try to learn from one source try to learn from different sources to get more ideas. Remove that thing from around you that creates distractions for you. And the most important thing is that practice for 20 hours not for a day but manage it in your own way.

**Question 6**

What are some of the steps that you can while approaching a new topic?

**Answer 6**

Steps while approaching a new topic:

- The first step is to need the tool which you want to learn.
- The second step is to deconstruct the topic. Part it into small pieces and find what is important.
- The third step is to learn the topic from different resources not only from one.
- The fourth step is to do more and more practice.
