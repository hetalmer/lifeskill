# **ABSTRACT**

This article introduces the use of Full-Text Search using Lucence. It contains an introduction about full-text search. It contains a detailed look at Lucence and how it works, how it takes less time to find the string, and how its architecture works with indexing. We also learn how Solr and Elasticsearch are used with Lucene and which is better to use with Lucene.

# **1\. Introduction**

Full-text search refers to searching some text inside extensive text data stored electronically and returning results that contain some or all of the words from the query. In contrast, traditional search would return exact matches. While traditional databases are great for storing and retrieving general data, performing full-text searches has been challenging. Frequently, additional tooling is required to achieve this. Many websites and application programs provide full text search capabilities.

Full-text search can have many different usages—for example, looking for a dish on a restaurant menu or looking for a specific feature in the description of an item on an e-commerce website. In addition to searching for particular keywords, you can augment a full-text search with search features like fuzzy-text and synonyms. Therefore, the results for a word such as “pasta” would return not only items such as “Pasta with meatballs” but could also return items like “Fettuccine Carbonara” using a synonym, or “Bacon and pesto flatbread” using a fuzzy search.

# **2\. Full Text Search**

Full-text search is meant to search large amounts of text. For example, a search engine will use a full-text search to look for keywords in all the web pages that it indexed. The key to this technique is indexing. Full text search engines are used widely. For example, Google allows users to find the needed query on web pages particularly with the help of this technique. If you have your own website with a lot of data, applying full text search might be very useful because it eases interaction for a user. Full text search may be useful when one needs to search for:

- a name of the person in a list or a database
- a word or a phrase in a document
- a web page on the internet
- products in an online store, etc.
- a regular expression

Following are the key points of full text search, with the use of that it become more popular and faster.

- Indexing
- Searching
- Filter and Sort the Result

Indexing is the most important feature to find the data. Large-scale information retrieval systems such as Google, Baidu take advantage of the approach of inverted index. After indexing the information, the searching is done by the user. User write information which user want to search and it submitted to retrieval system to process. It gives result and pass to filter process. Filter and Sorting process done with certain rules and the information given by user. After procedure the result return back to user.

# **3\. Full Text Search with Lucene**

There is many software available to make full text indexing and searching and Apache Lucene is one of them. Apache Lucene is a free and open-source search engine software library, originally written in Java by Doug Cutting. Apache Lucene is a powerful Java library used for implementing full-text search on a corpus of text. With its wide array of configuration options and customizability, it is possible to tune Apache Lucene specifically to the corpus at hand - improving both search quality and query capability.

Lucene has scalable and powerful indexing. With the use of that indexing and it has powerful, accurate and efficient search algorithms to search. The purpose of storing an index is to optimize speed and performance in finding relevant documents for a search query. With and index,  the search engine would scan every document in corpus, which would require considerable time and computing power.

For example, while an index f 10,000 documents can be queried within milliseconds, a sequential scan o every word in 10,000 large documents could take hours. The components of Lucene are described below:

![Lucene model](https://www.alachisoft.com/resources/docs/ncache/prog-guide/media/lucene-new.PNG)

In order to use Lucene for text searching, let us take a closer look at the main API used by Lucene. Following is the basic API used to build a Lucene based solution.

- Directory
- Document
- Analyzer
- IndexWriter
- IndexReader
- IndexSearcher

The directory is the base class where Lucene indexes are stored. A document is a collection of fields. These fields contain textual data as value against a field name. The value is the data that you want to index and then make searchable for future. With every field, user can specify whether he wants to analyze that field value or not.  Analyzer analyzing the data and converted into token. It basically parse the field of document into indexable token. List below are the most use Lucene analyzer.

- Whitespace Analyzer
- Standard Analyzer
- Simple Analyzer
- Stop Analyzer

Suppose we have a text “This is Lucene architecture at xyz.com", let's see how above analyzer works:

<table><tbody><tr><td><strong>Whitespace Analyzer</strong></td><td><strong>Standard Analyzer</strong></td><td><strong>Simple Analyzer</strong></td><td><strong>Stop Analyzer</strong></td></tr><tr><td><p>This</p><p>is</p><p>Lucene</p><p>architecture</p><p>at</p><p>xyz.com</p></td><td><p>Lucene</p><p>architectrue</p><p>xyz</p><p>com</p></td><td><p>this</p><p>is</p><p>lucene</p><p>architecture</p><p>at</p><p>xyz</p><p>com</p></td><td><p>lucene</p><p>architecture</p><p>xyz.com</p><p>&nbsp;</p></td></tr></tbody></table>

IndexWriter, IndexReader and IndexSearcher all are connected with one another. IndexWriter is used to write index file and it reference used to read the instruction using IndexReader. IndexSearcher is used to find the text from file and it uses reference of IndexReader to search the content to index file. Lucene itself is just an indexing and search library and does not contain crawling and HTML parsing functionality. However, many projects extend Lucene's capability and two of them are: Elasticsearch and Solr.

# **4\. Elasticsearch and Solr**

## **4.1 Elasticsearch**

Elasticsearch is an Apache Lucene-based search server. It was developed by Shay Banon and published in 2010. It is now maintained by Elasticsearch BV. Its latest version is 7.0.0.

Elasticsearch is a real-time distributed and open source full-text search and analytics engine. It is accessible from RESTful web service interface and uses schema less JSON (JavaScript Object Notation) documents to store data. It is built on Java programming language and hence Elasticsearch can run on different platforms. It enables users to explore very large amount of data at very high speed.

## **4.2 Solr**

Solr is an open-source search platform which is used to build search applications. It was built on top of Lucene (full text search engine). Solr is enterprise-ready, fast and highly scalable. The applications built using Solr are sophisticated and deliver high performance.

Yonik Seely who created Solr in 2004 in order to add search capabilities to the company website of CNET Networks. In Jan 2006, it was made an open-source project under Apache Software Foundation. Its latest version, Solr 6.0, was released in 2016 with support for execution of parallel SQL queries.

Solr can be used along with Hadoop. As Hadoop handles a large amount of data, Solr helps us in finding the required information from such a large source. Not only search, Solr can also be used for storage purpose. Like other NoSQL databases, it is a non-relational data storage and processing technology.

## **4.3 Which is better**

Both Elasticsearch and Solr has its own features and both features are very similar. However, they differ significantly in terms of deployment, scalability, query language, and many other functionalities. Solr has more advantages when it comes to the static data, because of its caches and the ability to use an uninverted reader for faceting and sorting – for example, e-commerce. On the other hand, Elasticsearch is better suited – and much more frequently used – for timeseries data use cases, like log analysis use cases. Documentation of Elasticsearch much easier than Solr.

So, we can say that both have many advantages and little disadvantages but both are in demand for full text searching.

# **5\. Conclusion**

Lucene is a full text indexing engine toolkit written in Java, multi-user support access, quickly visit indexing time and can cross-platform use. This paper in detail analyze the analysis of Lucene, indexing and searching three main modules from system architecture and what is the use of Elasticsearch and Solr.

# **6\. References**

- [https://www.scirp.org/html/4-83465_23718.htm](https://www.scirp.org/html/4-83465_23718.htm)
- [https://www.youtube.com/watch?v=vLEvmZ5eEz0&list=RDCMUCkw4JCwteGrDHIsyIIKo4tQ&start_radio=1&rv=vLEvmZ5eEz0&t=602](https://www.youtube.com/watch?v=vLEvmZ5eEz0&list=RDCMUCkw4JCwteGrDHIsyIIKo4tQ&start_radio=1&rv=vLEvmZ5eEz0&t=602)
- [https://www.tutorialspoint.com/elasticsearch/elasticsearch_basic_concepts.htm](https://www.tutorialspoint.com/elasticsearch/elasticsearch_basic_concepts.htm)
- [https://en.wikipedia.org/wiki/Full-text_search](https://en.wikipedia.org/wiki/Full-text_search)
- [https://www.lucenetutorial.com/basic-concepts.html](https://www.lucenetutorial.com/basic-concepts.html)
- [https://www.toptal.com/database/full-text-search-of-dialogues-with-apache-lucene](https://www.toptal.com/database/full-text-search-of-dialogues-with-apache-lucene)
- [https://www.alachisoft.com/resources/docs/ncache/prog-guide/lucene-components.html#document](https://www.alachisoft.com/resources/docs/ncache/prog-guide/lucene-components.html#document)
- [https://sematext.com/blog/solr-vs-elasticsearch-differences/](https://sematext.com/blog/solr-vs-elasticsearch-differences/)
