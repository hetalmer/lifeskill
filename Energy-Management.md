# Energy Management

## 1\. Manage Energy not Time

### Question 1:

What are the activities you do that make you relax - Calm quadrant?

- Talking with close one
- Listing song
- Reading Bhagvat Geeta

### Question 2:

When do you find getting into the Stress quadrant?

- Facing interview
- When family members are in tension

### Question 3:

How do you understand if you are in the Excitement quadrant?

- When  I  am happy to start new things.

## 4\. Sleep is your superpower

### Question 4

Paraphrase the Sleep is your Superpower video in detail.

Sleep is the most important part of daily life. We must have slept for at least 6 to 8 hours. If we did not sleep well, our minds would not work properly. Not only this, but there are so many side effects from not sleeping for 6 to 8 hours. A person who does not get enough sleep may experience memory loss, which is a sign of aging, and be unable to concentrate on anything. Lack of sleep is also a sign of depression.

### Question 5

What are some ideas that you can implement to sleep better?

- Stop thinking
- Reading good books
- Listen music
- Counting
- Do some yoga before sleep
- Meditation

## 5\. Brain Changing Benefits of Exercise

### Question 6

Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

It immediately affects your brain. A workout can increase your focus and attention. It can also increase your reaction time in any situation. Exercise actually produces new brain cells, which is useful for improving your long-term memory. It also helps your brain. A workout can increase your focus and attention. It can also increase your reaction time in any situation. It also improves your mood, but after a long time.

### Question 7

What are some steps you can take to exercise more?

- Make it routine
- Join yoga classes or gym
- Make it a habit
