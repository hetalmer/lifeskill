# Grit and Growth Mindset

# 1\. Grit

**Question 1**  
Paraphrase (summarize) the video in a few lines. Use your own words.

For me, one line for this video is to "not give up in any situation".Your success is not dependent on your IQ, looks, or health, or never fail before. It only depends on your mindset or grit. If you want to do something then, no one can stop you, not even you. Now the question is how to develop grit, so grit can't be developed by the people around you, it only be developed by the person inside you.

**Question 2**

What are your key takeaways from the video to take action on?

- Stop comparing, every child is intelligent with his/her unique quality.
- When someone fails at any stage try to encourage him/her, your small effort may change someone's life.
- The ability to learn is not fixed, it can change with your efforts.
- Don't be afraid to try new things, you will fail but you can learn a lot from it.

# 2\. Introduction to Growth Mindset

**Question 3**  
Paraphrase (summarize) the video in a few lines in your own words.

There are two types of mindsets that people have:

1.  growth mindset
2.  fixed mindset

A person with a fixed mind set didn't try some thing new. That person always lives in his comfort zone. While the growth min set person does not have a comfort zone. That person is always ready for new challenges and try to learn new things.

**Question 4**  
What are your key takeaways from the video to take action on?

- You are not just one or the others
- Belive and focus to create a real growth mindset
- Don't afraid by making effort, doing challanges, making mistakes and taken feedback from others

# 3\. Understanding Internal Locus of Control

**Question 5**  
What is the Internal Locus of Control? What is the key point in the video?

Locus of control is essentially the degree to which you believe you have control over your life. There are two types of locus of control

- Internal Locus of Control
- External Locus of Control

Internal locus of control means that people believe that they control factor of the outside that's why they did right. In the external locus of control people believe in hard work, they believe that their outcomes come from their hard work and extra efforts. Key poits of locus of control are:

- Don't blame the situation or people try to understand why it happened
- Your action and hard work affected your life.

# 4\. How to build a Growth Mindset

**Question 6**  
Paraphrase (summarize) the video in a few lines in your own words.

A growth mindset can be developed by using the belief. Believe in yourself that you can do it, so it automatically becomes easy to face any problem. Another is always a question about your assumption because what we assume is not always right. Make a curriculum of your dreams. Making this will help you to reach your goal.

**Question 7**  
What are your key takeaways from the video to take action on?

- When you try something new, you may get discouragement but don't get disappointed make it your strength and fight for it.
- Try to make your difficulty your strength by facing that difficulty.
- Don't be afraid to fail because it is an element of your path to reach your goal.

# 5\. Mindset - A MountBlue Warrior Reference Manual

**Question 8**  
What are one or more points that you want to take action on from the manual? (Maximum 3)

1.  I will take ownership of the projects assigned to me. Its execution, delivery and functionality is my sole responsibility.
2.  I will wear confidence in my body. I will stand tall and sit straight.
