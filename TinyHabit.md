# Tiny Habits

## 1\. Tiny Habits - BJ Fogg

### Question 1

Your takeaways from the video (Minimum 5 points)

- A tiny habit makes big changes in our lives.
- We must develop the habit of doing something we dislike.
- Always enjoy yourself after doing something because it gives encouragement.
- Continuing with your tiny habit until it becomes your routine
- Make a habit of changing your behavior.

## 2\. Tiny Habits by BJ Fogg - Core Message

### Question 2

Your takeaways from the video in as much detail as possible

Fogg says that by shrinking every new habit to its tiniest possible version, very little motivation is needed to do it. As a result, the more motivated you are, the more capable you are of completing any difficult task. A tiny habit is like growing a tree; it begins small but produces enormous results.

### Question 3

How can you use B = MAP to make making new habits easier?

MAP stands for motivation, ability, and prompt. Some things bother me, so I ignore them most of the time, which necessitates the use of tiny habit methods. For example, I don't like to design, but if I started with some tiny things, it might be useful for me.

### Question 4

Why it is important to "Shine" or Celebrate after each successful completion of habit?

We have to celebrate the little things because they really motivate us. The work may be small, but it will motivate more than that. I am celebrating that by clapping, and it really motivates me.

## 3\. 1% Better Every Day Video

### Question 5

Your takeaways from the video (Minimum 5 points)

It may appear insignificant to improve one percent, but it yields greater results over time. There are four stages to it: noticing, wanting, doing, and liking. Noticing implies that we must first recognise that I must do that thing before we can begin any task. Wanting means that we can do further processing on the thing if we really want it. After that, we must practise this thing, which may be unpleasant at times, but consistency is what is most important. We automatically like that thing after doing it for a long time, and we can't stop doing it.

## 4\. Book Summary of Atomic Habits

### Question 6

Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

Solve the problems on loger tems our ientity becomes changed. Identity, process, and outcomes are related to each other. Whatever we process, we get outcomes related to it based on our identity.

### Question 7

Write about the book's perspective on how to make a good habit easier?

We can make our habit easier by cure,craving,response and rewards. In short, by doing small habit, it gives huge change in over lives.

### Question 8

Write about the book's perspective on making a bad habit more difficult?

Make our bad habits more difficult by making the cues invisible, the action unattractive and hard, and the rewards unstatisfying in an ideal world.

## 5\. Reflection:

### Question 9:

Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I have a two habits which I want to do more:

- reading books other than gujarati language
- desining a website

II need to start small, like reading only one paragraph or a few sentences. I'll have to increase my reading after a while.

### Question 10:

Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I forgot to eat while doing work. So I need to make time management for that. And try to leave my snacks on the desk for me to eat.
