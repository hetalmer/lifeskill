# What kinds of behaviour cause sexual harassment?

##### Any unwelcome verbal, visual or physical conduct of a sexual nature that is severe or pervasive affects working conditions or creates a hostile work environment. There are generally three forms of sexual harassment

- Verbal harassment is like comments about clothing, comments about someone's personal and sexual life, comments about a person's body, asking about sexual relations, or repeatedly asking about the same thing again and again.
- Visual harassment includes obscene posters, pictures, screen servers, emails, or text.
- Physical harassment includes sexual assault, and inappropriate touching, such as kissing, sexual gesturing, and staring

Each of these harassments divided into two categories

1.  Quid Pro Quo : This means that "this for that". This means an employee asked by his/her supervisor for sexual relations for promotion.
2.  Hostile Work Environment : Occurs when employee behavior interferes with the work performance of another or creates an intimidating or offensive workplace.

# What would you do in case you face or witness any incident or repeated incidents of such behaviour?

##### If I faced such kind of behavior I will do nothing. I am not that brave enough to fight for myself or for others. I just ignore that or leave that place that feels me uncomfortable. From my experience, I have learned that you can't fight for everybody if you want mental peace then just ignore them.
